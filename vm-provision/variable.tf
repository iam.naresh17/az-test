variable "resourcename" {
  description = "resource group name."
}

variable "location" {
  description = "Location of the resource group."
}

variable "nsgname" {
}

variable "nsgrule" {
}

variable "address_space" {
}

variable "vnetname" {
}
variable "subscription_id" {
  default = ""
}
variable "client_id" {
  default = ""
}
variable "client_secret" {
  default = ""
}
variable "tenant_id" {
  default = ""
}
